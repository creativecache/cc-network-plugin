<?php
/**
 * Plugin Name: Creative Cache Default Plugins
 * Plugin URI: https://creativecache.co
 * Description: Required default plugins for all websites hosted by Creative Cache.
 * Version: 1.0
 * Author: Creative Cache
 * Author URI: https://creativecache.co
 */

defined( 'ABSPATH' ) or die( 'No script please!' );

// Add Creative Cache Google Analytics to all subsites
function google_analytics_tracking_code(){

	$propertyID = 'UA-98835640-1'; // GA Property ID ?>

<!-- Global site tag (gtag.js) for Creative Cache - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $propertyID; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?php echo $propertyID; ?>');
</script>

<?php }

add_action('wp_head', 'google_analytics_tracking_code');